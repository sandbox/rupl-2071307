# Multilingual Menu Parent

This module provides convenient default on node translation forms. It is only
useful if you have a hierarchical menu, and the menu is set up properly in the
site's default languge.

When a new translation node is created, it checks the default language, crawls
the menu structure, and finds the plid that your new node should likely have.

Requires: menu_node
https://drupal.org/project/menu_node

Original author: Chris Ruppel
https://drupal.org/user/411999
